#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/readtheorg.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/readtheorg/js/readtheorg.js"></script>
#+HTML_HEAD: <style>pre.src { color: white; background-color: #333 }</style>

* TD Devops

** Projet de demarrage

*** Forker/cloner le repository Git de base

- creer un compte sur gitlab.com
- aller sur le projet https://gitlab.com/eauc/support-devops-insa.git
- clicker sur le bouton "Fork" pour copier le projet dans votre espace perso gitlab.
- cloner votre projet perso sur votre machine de dev.

#+BEGIN_SRC sh
git clone <url du projet>
#+END_SRC

*** Rappels quelques commandes Git

Dans le repertoire du projet
#+BEGIN_SRC sh
git status # voir les modifications locales des fichiers du projet
git commit -a # creer un nouveau commit avec toutes les modification locales
git push origin <branch> # pousser les nouveaux commits crees localement sur le repository gitlab
git pull # recuperer les nouveaux commits sur le serveur gitlab
git <cmd> --help # aide pour n'importe quelle commande git
#+END_SRC


** Docker sur le poste de developpement

*** Installer Docker CE

Exemple sur une Debian:
[[https://docs.docker.com/engine/installation/linux/docker-ce/debian/][Doc officielle]]

*** Docker worklflow

[[./docker_flow.png]]

[[./docker_project.png]]

*** Image node de base

- aller sur le [[https://hub.docker.com/][Docker Hub]].
- trouver l'image officielle Node.
- creer un fichier Dockerfile dans le projet, basé sur l'image officielle node pour la version specifiee dans les tests.
- executer un shell node dans un container basé sur cette image.
- verifier que la version de Node est bien celle desiree.

Directives Dockerfile:
- =FROM <image:version>= :: base l'image construite sur une autre image Docker.

Commandes docker:
- =docker build <dir>= :: construit une image docker a partir du Dockerfile situe dans un repertoire.
   - l'option =-t= :: permet d'affecter un tag (nom) a l'image.
- =docker images= :: liste les images presentes sur le docker local.
- =docker rmi <image>= :: efface une image docker.
- =docker run <image>= :: cree et demarre un container base sur une image docker.
   - l'option =-i= :: (interactive) garde le stdin du container ouvert et le connecte au stdin du terminal.
   - l'option =-t= :: (tty) le stdin se comporte comme un terminal TTY.
   - l'option =--name <name>= :: nomme le container (plus pratique pour l'arreter, etc).
   - l'option =--rm= :: efface le container lorsque son execution se termine.
   - l'option =-d= :: (daemon) detache le container du terminal.
- =docker ps= :: liste les containers presents sur la plateforme docker locale.
  - l'option =-a= :: liste les containers ayant termine leur execution.
- =docker start <container>= :: demarre un container precedemment cree par =docker run=.
- =docker stop <container>= :: arrete un container.
- =docker rm <container>= :: efface un container.

Commande Node:
- =process.version= :: affiche la version courante de node.

*** Image de developpement

- inclure les sources du client dans l'image du projet.
- executer un shell bash dans un container base sur l'image pour verifier que les sources sont bien incluses dans l'image.
- installer les dependances node dans l'image du projet.
- executer un shell bash dans un container base sur l'image pour verifier que les dependances sont bien incluses dans l'image.
- demarrer le client node automatiquement au lancement du container.
- lancer le container (sans surcharger la commande de demarrage, et en publiant le port 3000 sur localhost) pour verifier que le client est bien demarre.
- charger [[http://localhost:3000]] dans un navigateur (ou CURL) pour verifier que le client repond correctement.

Directives Dockerfile:
- =COPY <src> <dst>= :: copie tout le contenu du repertoire <src> de la machine hote (relatif au Dockerfile), dans le repertoire <dst> du file system de l'image docker.
- =WORKDIR <dir>= :: change le repertoire courant dans le file system de l'image docker (~ =cd <dir>=).
  - affecte les directives suivantes du Dockerfile.
  - les commandes executees dans les containers seront egalement lancees depuis ce repertoire.
- =RUN <cmd arg...>= :: execute une commande, durant la construction de l'image, dans le file system de l'image.
- =CMD ["<cmd>", "<arg1>", "<arg2>", ...]= :: definit la commande par defaut executee au lancement d'un container.
  - eg. =CMD ["echo", "toto"]= - il faut passer un tableau de Strings separees par des virgules !

Commande docker:
- =docker run <image> <cmd>= :: execute un container base sur <image>, en remplacement la commande par defaut par <cmd>.
  - l'option =-p <port-container>:<port-hote>= :: permet de publier un port TCP du container sur la machine hote.

Commandes node:
- =npm install= :: installe les dependances node decrites dans le =package.json= du projet.
  - les dependances sont installees dans le repertoire =node_modules= a la racine du projet.
- =npm start= :: demarre le serveur node.

*** Setup de developpement

- executer un container de developpement, en montant le repertoire du projet sur la machine hote comme un volume dans le container, de sorte que les sources en cours d'edition soient visibles dans le container.
- lancer le client dans le container avec nodemon.
- verifier que lorsqu'un fichier source est modifie, le cient redemarre dans le container.

Commandes docker:
- =docker run <image> <cmd>= ::
  - l'option =-v <repertoire-hote>:<repertoire-container>= :: permet de "monter" un repertoire de la machine hote a la place d'un repertoire dans le container. Le container peut ainsi "voir" le contenu d'un repertoire de la machine hote.

Commandes node:
- =npm install --save-dev <package>= :: installe une dependance de developpement et la rajoute au =package.json= du projet.
- =./node_modules/.bin/nodemon .= :: execute le serveur node du projet, le redemarre si une source est modifiee.

** Setup CI/CD

   Lorsque vous poussez un projet sur Gitlab.com avec un fichier =.gitlab-ci.yml=, la chaine de CI de gitlab est automatiquement lancee sur le commit pousse.

   [[https://docs.gitlab.com/ce/ci/yaml/README.html][Doc du fichier gitlab-ci]]

   Ce projet contient un embryon de fichier =.gitlab.ci.yml= a utiliser comme base de depart pour le TD. Il comporte notament les definitions de plusieurs variables d'environnement necessaire a l'utilisation de docker dans la CI Gitlab.

   Le fichier contient plusieurs sections:

*** Stages

    On peut definir plusieurs etapes qui seront executees sequentiellement par la CI.

    #+BEGIN_SRC yaml
    stages:
      - build
      - test
      - deploy
    #+END_SRC
    Dans cet example, chaque "pipeline" de CI comprendra 3 etapes sequentielles : build > test > deploy

*** Jobs

    Les "jobs" sont des taches qui s'executent lors de chaque "stage" du pipeline. Si un job echoue, les "stages" suivants du pipeline sont ignores et le pipelin de CI echoue a cette etape.

    On peut definir un job de la facon suivante :
    #+BEGIN_SRC yaml
    myJob:
      stage: build
      script:
        - docker build -t $CONTAINER_TEST_IMAGE .
    #+END_SRC
    Dans cet example, =myJob= sera execute durant l'etape de =build=. La propriete =script= est une liste de commandes docker a executer pour accomplir cette tache. Ici on construit simplement une image, nommee a partir d'une variable d'environnement.

*** Heroku

    Heroku est une plateforme Saas qui permet notament de deployer des serveurs gratuitement, bases sur une image docker.

    Le deploiement consiste en 3 etapes:
    - construire une image docker executant le serveur.
    - pousser cette image docker dans le repository heroku =registry.heroku.com=.
    - publier ("release") cette image pour que le container serveur redemarre en l'utilisant.

**** Creer un compte Heroku

     [[https://www.heroku.com/]]

     - recuperer votre clef d'API: Accounts Settings > Account > API Key
     - la copier dans les variables d'environnement de votre projet Gitlab.com: Settings > CI/CD > Variables. L'appeler par example =HEROKU_AUTH_TOKEN=.

**** Pousser une image dans le repository Heroku

     - il faut tout d'abord "tagger" l'image avec le nom du registry Heroku, le nom de l'application visee, et le type de process - web dans notre cas.
     - ensuite il faut se login sur le registry Heroku, grace au token d'authentification (obtenue dans la console d'administration web du compte Heroku, Account Settings > Account > API Key)
     - ensuite on peut simplement pousser l'image dans le repository Heroku.

     #+BEGIN_SRC sh
     docker tag <my-image> registry.heroku.com/<app-name>/web
     docker login --username=_ --password=<HEROKU_AUTH_TOKEN> registry.heroku.com
     docker push registry.heroku.com/<app-name>/web
     #+END_SRC

**** Publier l'image

     Ca s'invente pas, on peut utiliser la commande suivante:
     #+BEGIN_SRC sh
     docker run --rm -e HEROKU_API_KEY=$HEROKU_AUTH_TOKEN wingrunr21/alpine-heroku-cli container:release web --app <app-name>
     #+END_SRC

**** Voir les logs du client dans Heroku

     Dans le dashboard de l'application, en haut a droite More > View logs.

     Acces au log du client comme en local.

     On peut aussi installer l'outil Heroku CLI : [[https://devcenter.heroku.com/articles/heroku-cli]] qui permet de visualiser les logs d'une appli en ligne de commande.
     #+BEGIN_SRC sh
     heroku login
     heroku logs -t --app <app-name>
     #+END_SRC

*** Consignes

    - completer le fichier =.gitlab-ci.yml= pour realiser 3 etapes : build > test > deploy
      - l'etape build doit construire une image de votre client et la pousser dans le repository gitlab (le fichier fourni initialise les variables necessaires, et realise le login dans ce repository).
      - l'etape de test doit recuperer l'image construite a l'etape precendente, et executer les tests unitaires dans un container utilisant cette image.
      - l'etape de deploiement doit recuperer l'image, et la deployer dans Heroku comme explique ci-dessus.
    - tester ce deploiement continu (il est fortement conseille de proceder par... etapes progressives).
    - une fois la CI fonctionnelle, enregistrer votre client Heroku dans le serveur Heroku a cette addresse : [[https://eauc-xtrem-carpaccio.herokuapp.com/]]
    - implementer chaque feature de l'extreme carpaccio, pour arriver a un client completement fonctionnel.
      - faire un commit a chaque etape.
      - si vous avez le temps, ecrire des tests unitaires validant chaque fonctionnalite (des tests de base existent deja dans le repertoire [[./test]]).

    Pour le rapport de TD, ecrire un document expliquant a un nouveau membre de l'equipe:
    - comment fonctionne la CI,
    - comment lancer un container de developpement,
    - comment deployer le projet dans Gitlab.
